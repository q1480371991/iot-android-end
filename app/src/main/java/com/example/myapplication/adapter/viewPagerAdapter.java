package com.example.myapplication.adapter;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.PagerAdapter;

import com.example.myapplication.fragment.CurrentFragment;
import com.example.myapplication.fragment.HistoricalFragment;

import org.w3c.dom.Text;

public class viewPagerAdapter extends FragmentPagerAdapter {

    public viewPagerAdapter(@NonNull FragmentManager fm) {
        super(fm,BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        Log.i("liangxulin", "position:"+position);
        if (position==0) {
            return CurrentFragment.newInstance();
        }
        else if (position==1) {
            return HistoricalFragment.newInstance();
        }
        return CurrentFragment.newInstance();
    }

    @Override
    public int getCount() {
        return 2;
    }
}
