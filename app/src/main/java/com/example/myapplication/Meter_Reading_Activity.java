package com.example.myapplication;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;

import com.example.myapplication.adapter.viewPagerAdapter;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.qmuiteam.qmui.widget.QMUITopBar;
import com.qmuiteam.qmui.widget.QMUIViewPager;
import com.qmuiteam.qmui.widget.tab.QMUITab;
import com.qmuiteam.qmui.widget.tab.QMUITabBuilder;
import com.qmuiteam.qmui.widget.tab.QMUITabSegment;
import com.qmuiteam.qmui.widget.tab.QMUITabView;

public class Meter_Reading_Activity extends AppCompatActivity {
    private QMUITopBar mTopBar;
    private QMUIViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meter_reading);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mTopBar=findViewById(R.id.h_dtopbar);
        QMUIStatusBarHelper.translucent(this);// 沉浸式状态栏
        initTopBar();//初始化状态栏

        QMUITabSegment tabSegment = findViewById(R.id.mr_tabs);

        // 创建标签

        QMUITabBuilder tabBuilder = tabSegment.tabBuilder();
        tabSegment.setBackgroundColor(ContextCompat.getColor(this, R.color.blue1));
        QMUITabView qmuiTabView = new QMUITabView(this);

        QMUITab tab1 = tabBuilder.setText("当前数据")

//                .setNormalDrawable()设置图标
                .setNormalColor(ContextCompat.getColor(this, R.color.grey))
                .setSelectColor(ContextCompat.getColor(this, R.color.blue))
                .build(this);

        QMUITab tab2 = tabBuilder.setText("历史数据")
                .setNormalColor(ContextCompat.getColor(this, R.color.grey))
                .setSelectColor(ContextCompat.getColor(this, R.color.blue))
                .build(this);



        // 将标签添加到QMUITabSegment中
        viewPager = findViewById(R.id.mr_viewpager);
        tabSegment.addTab(tab1);
        tabSegment.addTab(tab2);
        int tabCount = tabSegment.getTabCount();
        Log.i("liangxulin", String.valueOf(tabCount));

        tabSegment.setupWithViewPager(viewPager, false);


        viewPager.setAdapter(new viewPagerAdapter(getSupportFragmentManager()));
        viewPager.setCurrentItem(0);

    }

    private void initTopBar() {
        mTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.blue));
        mTopBar.setTitleGravity(Gravity.LEFT);
        mTopBar.addLeftBackImageButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
//                overridePendingTransition(R.anim.slide_still, R.anim.slide_out_right);
            }
        });

        mTopBar.setTitle("抄表");
    }
}