package com.example.myapplication;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.example.myapplication.util.MyQMUIDialog;
import com.github.chrisbanes.photoview.PhotoView;
import com.qmuiteam.qmui.widget.dialog.QMUIBottomSheet;

public class ZoomActivity extends AppCompatActivity {
    private ImageView zoomImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoom);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        zoomImageView = findViewById(R.id.zoomImageView);

        // 获取传递的图片资源ID
//        String imageRes = getIntent().getStringExtra("imageResId");
        String imageRes = getIntent().getStringExtra("imageResId");
//        Integer imageRes = getIntent().getIntExtra("imageResId",0);

//        Integer integer = Integer.valueOf(imageRes);
        if (imageRes != null) {
//            zoomImageView.setImageResource(imageResId);
            Glide.with(this)
                    .load(imageRes)
                    .into(zoomImageView);
        }
        zoomImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        zoomImageView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                QMUIBottomSheet.BottomGridSheetBuilder builder = new QMUIBottomSheet.BottomGridSheetBuilder(ZoomActivity.this);
                builder.addItem(R.drawable.img3, "QQ", 1, QMUIBottomSheet.BottomGridSheetBuilder.FIRST_LINE);
                builder.addItem(R.drawable.img4, "微信", 2, QMUIBottomSheet.BottomGridSheetBuilder.FIRST_LINE);
                builder.addItem(R.drawable.img2, "保存", 3, QMUIBottomSheet.BottomGridSheetBuilder.FIRST_LINE);
                builder.setOnSheetItemClickListener(new QMUIBottomSheet.BottomGridSheetBuilder.OnSheetItemClickListener() {
                    @Override
                    public void onClick(QMUIBottomSheet dialog, View itemView) {

                        int position = (int) itemView.getTag();
                        switch (position){
                            case 1:
                                //分享到qq
                                Toast.makeText( ZoomActivity.this,"点击了QQ", Toast.LENGTH_SHORT).show();
                                break;
                            case 2:
                                //分享到微信
                                Toast.makeText( ZoomActivity.this,"点击了微信", Toast.LENGTH_SHORT).show();
                                break;
                            case 3:
                                //保存图片

                                Toast.makeText( ZoomActivity.this,"点击了保存", Toast.LENGTH_SHORT).show();
                                break;
                        }
                        dialog.dismiss();
                    }
                });
//                MyQMUIDialog.MessageDialogBuilder(ZoomActivity.this);
                QMUIBottomSheet sheet = builder.build();
                sheet.show();
                return true;
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out); // 应用过渡动画
    }


}
