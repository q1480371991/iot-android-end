package com.example.myapplication;

import static com.example.myapplication.util.analog_Data.getHistoricalData;

import androidx.annotation.ColorInt;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupMenu;

import com.example.myapplication.adapter.viewPagerAdapter;
import com.example.myapplication.pojo.IotDataObj;
import com.qmuiteam.qmui.widget.QMUIViewPager;
import com.qmuiteam.qmui.widget.dialog.QMUIDialogMenuItemView;
import com.qmuiteam.qmui.widget.grouplist.QMUICommonListItemView;
import com.qmuiteam.qmui.widget.grouplist.QMUIGroupListView;
import com.qmuiteam.qmui.widget.tab.QMUITab;
import com.qmuiteam.qmui.widget.tab.QMUITabBuilder;
import com.qmuiteam.qmui.widget.tab.QMUITabSegment;
import com.qmuiteam.qmui.widget.tab.QMUITabView;

import java.util.ArrayList;

public class TestActivity extends AppCompatActivity implements View.OnClickListener {
    private QMUIViewPager viewPager;
    private ArrayList<IotDataObj> datas;
    private ArrayList<PopupMenu> menus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        QMUIGroupListView groupListView = findViewById(R.id.groupListView);
        menus=new ArrayList<PopupMenu>();
        datas=getHistoricalData();
        // 创建一个Section，并设置标题
        QMUIGroupListView.Section section = QMUIGroupListView.newSection(this)
                .setTitle("Historical Data");
        for (int i = 0; i < datas.size(); i++) {
            QMUICommonListItemView itemView = groupListView.createItemView("data "+i);
            itemView.setTag(i);
            PopupMenu popupMenu = new PopupMenu(TestActivity.this, itemView);

            // 添加菜单项
            popupMenu.getMenu().add(Menu.NONE, 1, Menu.NONE, "温度:"+datas.get((Integer) itemView.getTag()).getTemperature()+"°C");
            popupMenu.getMenu().add(Menu.NONE, 2, Menu.NONE, "湿度:"+datas.get((Integer) itemView.getTag()).getHumidity()+"%");
            popupMenu.getMenu().add(Menu.NONE, 3, Menu.NONE, "气压:"+datas.get((Integer) itemView.getTag()).getHyVal()+"pa");
            popupMenu.getMenu().add(Menu.NONE, 4, Menu.NONE, "光照强度:"+datas.get((Integer) itemView.getTag()).getAmVal()+"Lux");
            popupMenu.getMenu().add(Menu.NONE, 5, Menu.NONE, "CO2浓度:"+datas.get((Integer) itemView.getTag()).getDustVal()+"ppm");

            popupMenu.getMenu().add(Menu.NONE, 6, Menu.NONE, "时间:"+datas.get((Integer) itemView.getTag()).getTimestamp());
            // 设置菜单项点击事件
            popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    // 处理菜单项点击事件
                    switch (item.getItemId()) {
                        case 1:
                            // 菜单项1的处理逻辑
                            break;
                        case 2:
                            // 菜单项2的处理逻辑
                            break;
                        case 3:
                            // 菜单项3的处理逻辑
                            break;
                        case 4:
                            // 菜单项3的处理逻辑
                            break;
                        case 5:
                            // 菜单项3的处理逻辑
                            break;
                        case 6:
                            // 菜单项3的处理逻辑
                            break;
                    }
                    return true;
                }
            });
            menus.add(popupMenu);
            section.addItemView(itemView, this);
        }
        // 将Section添加到GroupListView中
        section.addTo(groupListView);







    }

    @Override
    public void onClick(View view) {
        Log.i("liangxulin", view.getTag().toString());
        menus.get((Integer) view.getTag()).show();
    }
}