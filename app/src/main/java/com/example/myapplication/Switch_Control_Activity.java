package com.example.myapplication;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.qmuiteam.qmui.widget.QMUITopBar;
import com.suke.widget.SwitchButton;

public class Switch_Control_Activity extends AppCompatActivity {

    private SwitchButton switchButton1;
    private SwitchButton switchButton2;
    private QMUITopBar mTopBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_switch_control);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mTopBar=findViewById(R.id.s_ctopbar);
        QMUIStatusBarHelper.translucent(this);// 沉浸式状态栏
        initTopBar();//初始化状态栏

        TextView nf1 = findViewById(R.id.nf_txt);
        TextView nf2 = findViewById(R.id.nf1_txt);
        switchButton1 = (com.suke.widget.SwitchButton)
                findViewById(R.id.switch_button1);
        switchButton2 = (com.suke.widget.SwitchButton)
                findViewById(R.id.switch_button2);

        switchButton1.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                Log.i("liangxulin", String.valueOf(isChecked));
                if (isChecked) {
                    nf1.setTextColor(ContextCompat.getColor(Switch_Control_Activity.this, R.color.green));
                    nf1.setText("ON");
                }
                else{
                    nf1.setTextColor(ContextCompat.getColor(Switch_Control_Activity.this, R.color.red));
                    nf1.setText("OFF");
                }

            }
        });

        switchButton2.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                Log.i("liangxulin", String.valueOf(isChecked));
                if (isChecked) {
                    nf2.setTextColor(ContextCompat.getColor(Switch_Control_Activity.this, R.color.green));
                    nf2.setText("ON");
                }
                else{
                    nf2.setTextColor(ContextCompat.getColor(Switch_Control_Activity.this, R.color.red));
                    nf2.setText("OFF");
                }

            }
        });


    }
    private void initTopBar() {
        mTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.blue));
        mTopBar.setTitleGravity(Gravity.LEFT);
        mTopBar.addLeftBackImageButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
//                overridePendingTransition(R.anim.slide_still, R.anim.slide_out_right);
            }
        });

        mTopBar.setTitle("开关控制");
    }
}