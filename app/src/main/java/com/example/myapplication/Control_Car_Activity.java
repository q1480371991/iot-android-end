package com.example.myapplication;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.qmuiteam.qmui.alpha.QMUIAlphaImageButton;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.qmuiteam.qmui.widget.QMUITopBar;

public class Control_Car_Activity extends AppCompatActivity {
    private QMUITopBar mTopBar;
    private String status="启动";//初始化小车状态
    private Button onoff;
    private QMUIAlphaImageButton up;
    private QMUIAlphaImageButton right;
    private QMUIAlphaImageButton down;
    private QMUIAlphaImageButton left;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ImageButton imageButton = new ImageButton(this  );
        imageButton.setClickable(true);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control_car);
        up=findViewById(R.id.up);
        right=findViewById(R.id.right);
        down=findViewById(R.id.down);
        left=findViewById(R.id.left);
        onoff=findViewById(R.id.onoff);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mTopBar=findViewById(R.id.c_ctopbar);
        QMUIStatusBarHelper.translucent(this);// 沉浸式状态栏
        initTopBar();//初始化状态栏


        Typeface iconfont = Typeface.createFromAsset(getAssets(), "font2.ttf");
        onoff.setTypeface(iconfont);
        onoff.setText(status);
        onoff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onoff.getText().equals("启动")){
                    changeButtonStatus(true);
                }else{
                    changeButtonStatus(false);
                }

            }
        });

    }









    private void initTopBar() {
        mTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.blue));
        mTopBar.setTitleGravity(Gravity.LEFT);
        mTopBar.addLeftBackImageButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
//                overridePendingTransition(R.anim.slide_still, R.anim.slide_out_right);
            }
        });

        mTopBar.setTitle("控制小车");
    }
    void changeButtonStatus(Boolean status)
    {
        if (status){
            this.onoff.setText("关闭");
        } else{
            this.onoff.setText("启动");
        }
        this.up.setClickable(status);
        this.down.setClickable(status);
        this.left.setClickable(status);
        this.right.setClickable(status);
    }
}