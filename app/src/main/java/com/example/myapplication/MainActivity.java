package com.example.myapplication;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.github.chrisbanes.photoview.PhotoView;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.qmuiteam.qmui.widget.QMUIRadiusImageView;
import com.qmuiteam.qmui.widget.QMUITopBar;
import com.qmuiteam.qmui.widget.roundwidget.QMUIRoundButton;

public class MainActivity extends AppCompatActivity {
    private QMUITopBar mTopBar;
    private PhotoView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActionBar actionBar = getSupportActionBar();

//        QMUIRoundButton qmuiRoundButton = findViewById(R.id.qmui_button);
//        qmuiRoundButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//            }
//        });


//        imageView = findViewById(R.id.imageView);

        // 假设imageUrl为后端传来的图片URL
        String imageUrl = "http://101.34.248.186:8080/mypath/java/imgs/1.jpg";

        RequestOptions requestOptions = new RequestOptions();


//        Glide.with(this)
//                .load(imageUrl)
//                .placeholder(R.drawable.loading)
//                .error(R.drawable.error)
//                .apply(requestOptions)
//                .thumbnail(0.1f)
//                .into(imageView);
//        imageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(MainActivity.this, ZoomActivity.class);
//                intent.putExtra("imageResId", imageUrl);
//                startActivity(intent);
//                overridePendingTransition(0, 0); // 禁用默认的切换动画
//            }
//        });

//        mTopBar=findViewById(R.id.topbar);
//        QMUIStatusBarHelper.translucent(this);// 沉浸式状态栏
//        initTopBar();//初始化状态栏


//        qmuiRoundButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                MyQMUIDialog.MessageDialogBuilder(MainActivity.this);
//                Intent intent = new Intent();
//                intent.setClass(MainActivity.this, TestActivity.class);
//                startActivity(intent);
//
//            }
//        });

    }
    private void initTopBar() {
        mTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.green));
        mTopBar.addLeftBackImageButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
//                overridePendingTransition(R.anim.slide_still, R.anim.slide_out_right);
            }
        });

        mTopBar.setTitle("沉浸式状态栏示例");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public  boolean onOptionsItemSelected(MenuItem item){

        int itemId = item.getItemId();
        Intent intent = new Intent();
        Log.i("liangxulin", String.valueOf(itemId));
        Log.i("liangxulin", String.valueOf(R.id.current_environment));
        if(itemId==R.id.current_environment){
            intent.setClass(MainActivity.this, Current_Environment_Activity.class);
            startActivity(intent);
        } else if (itemId==R.id.environmental_status) {
            intent.setClass(MainActivity.this, Environmental_Status_Activity.class);
            startActivity(intent);
        } else if (itemId==R.id.control_car) {

            intent.setClass(MainActivity.this, Control_Car_Activity.class);
            startActivity(intent);
        }else if (itemId==R.id.switch_control) {

            intent.setClass(MainActivity.this, Switch_Control_Activity.class);
            startActivity(intent);
        }else if (itemId==R.id.line_detection) {

            intent.setClass(MainActivity.this, Line_Detection_Activity.class);
            startActivity(intent);
        }else if (itemId==R.id.meter_reading) {
            intent.setClass(MainActivity.this, Meter_Reading_Activity.class);
            startActivity(intent);
        }else if (itemId==R.id.car_perspective) {
            intent.setClass(MainActivity.this, Car_Perspective_Activity.class);
            startActivity(intent);
        } else if (itemId==R.id.historical_data) {
            intent.setClass(MainActivity.this, Historical_Data_Activity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}