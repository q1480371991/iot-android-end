package com.example.myapplication.pojo;

import com.github.chrisbanes.photoview.PhotoView;
import com.qmuiteam.qmui.widget.QMUIVerticalTextView;

public class Line_ViewHolder {
    private PhotoView photoView;
    private QMUIVerticalTextView textView;

    public QMUIVerticalTextView getTextView() {
        return textView;
    }

    public void setTextView(QMUIVerticalTextView textView) {
        this.textView = textView;
    }

    public PhotoView getPhotoView() {
        return photoView;
    }

    public void setPhotoView(PhotoView photoView) {
        this.photoView = photoView;
    }

    public Line_ViewHolder() {
    }

    public Line_ViewHolder(PhotoView photoView, QMUIVerticalTextView textView) {
        this.photoView = photoView;
        this.textView = textView;
    }
}
