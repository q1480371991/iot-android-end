package com.example.myapplication.pojo;

import android.widget.TextView;

import com.qmuiteam.qmui.widget.QMUIRadiusImageView;

public class Meter_ViewHolder {
    private QMUIRadiusImageView img;
    private TextView name;
    private TextView data;

    public Meter_ViewHolder() {
    }

    public Meter_ViewHolder(QMUIRadiusImageView img, TextView name, TextView data) {
        this.img = img;
        this.name = name;
        this.data = data;
    }

    public QMUIRadiusImageView getImg() {
        return img;
    }

    public void setImg(QMUIRadiusImageView img) {
        this.img = img;
    }

    public TextView getName() {
        return name;
    }

    public void setName(TextView name) {
        this.name = name;
    }

    public TextView getData() {
        return data;
    }

    public void setData(TextView data) {
        this.data = data;
    }
}
