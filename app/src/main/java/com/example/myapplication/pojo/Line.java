package com.example.myapplication.pojo;


/**
 * @author : Lin
 * @version : [v1.0]
 * @className : Line
 * @description : 描述说明该类的功能
 * @createTime : 2023/10/8 13:47
 * @updateUser : Lin
 * @updateTime : 2023/10/8 13:47
 * @updateRemark : 描述说明本次修改内容
 */

public class Line {



    private Integer id;
    private String imgurl;

    public Line() {
    }

    public Line(Integer id, String imgurl, Integer isdeal, String roomid, String status) {
        this.id = id;
        this.imgurl = imgurl;
        this.isdeal = isdeal;
        this.roomid = roomid;
        this.status = status;
    }

    private Integer isdeal;
    private String roomid;
    private String status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    public Integer getIsdeal() {
        return isdeal;
    }

    public void setIsdeal(Integer isdeal) {
        this.isdeal = isdeal;
    }

    public String getRoomid() {
        return roomid;
    }

    public void setRoomid(String roomid) {
        this.roomid = roomid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Line{" +
                "id=" + id +
                ", imgurl='" + imgurl + '\'' +
                ", isdeal=" + isdeal +
                ", roomid='" + roomid + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
