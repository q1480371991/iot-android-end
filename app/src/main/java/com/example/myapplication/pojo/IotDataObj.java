package com.example.myapplication.pojo;


/**
 * @author : Lin
 * @version : [v1.0]
 * @className : IotDataObj
 * @description : 描述说明该类的功能
 * @createTime : 2023/7/2 19:20
 * @updateUser : Lin
 * @updateTime : 2023/7/2 19:20
 * @updateRemark : 描述说明本次修改内容
 */


public class IotDataObj {


    private Integer id;
    private String eleBrake;
    private String ammonia_warning;
    private Integer temperature;

    private Integer humidity;

    private String fire_Warning;

    private String smoke_Warning;

    private String hydrogen_warning;

    private Integer hyVal;

    private Integer amVal;

    private Integer dustVal;
    private String timestamp;



    public IotDataObj() {
    }

    @Override
    public String toString() {
        return "IotDataObj{" +
                "id=" + id +
                ", eleBrake='" + eleBrake + '\'' +
                ", ammonia_warning='" + ammonia_warning + '\'' +
                ", temperature=" + temperature +
                ", humidity=" + humidity +
                ", fire_Warning='" + fire_Warning + '\'' +
                ", smoke_Warning='" + smoke_Warning + '\'' +
                ", hydrogen_warning='" + hydrogen_warning + '\'' +
                ", hyVal=" + hyVal +
                ", amVal=" + amVal +
                ", dustVal=" + dustVal +
                ", timestamp='" + timestamp + '\'' +
                '}';
    }

    public IotDataObj(Integer id, String eleBrake, String ammonia_warning, Integer temperature, Integer humidity, String fire_Warning, String smoke_Warning, String hydrogen_warning, Integer hyVal, Integer amVal, Integer dustVal, String timestamp) {
        this.id = id;
        this.eleBrake = eleBrake;
        this.ammonia_warning = ammonia_warning;
        this.temperature = temperature;
        this.humidity = humidity;
        this.fire_Warning = fire_Warning;
        this.smoke_Warning = smoke_Warning;
        this.hydrogen_warning = hydrogen_warning;
        this.hyVal = hyVal;
        this.amVal = amVal;
        this.dustVal = dustVal;
        this.timestamp = timestamp;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEleBrake() {
        return eleBrake;
    }

    public void setEleBrake(String eleBrake) {
        this.eleBrake = eleBrake;
    }

    public String getAmmonia_warning() {
        return ammonia_warning;
    }

    public void setAmmonia_warning(String ammonia_warning) {
        this.ammonia_warning = ammonia_warning;
    }

    public Integer getTemperature() {
        return temperature;
    }

    public void setTemperature(Integer temperature) {
        this.temperature = temperature;
    }

    public Integer getHumidity() {
        return humidity;
    }

    public void setHumidity(Integer humidity) {
        this.humidity = humidity;
    }

    public String getFire_Warning() {
        return fire_Warning;
    }

    public void setFire_Warning(String fire_Warning) {
        this.fire_Warning = fire_Warning;
    }

    public String getSmoke_Warning() {
        return smoke_Warning;
    }

    public void setSmoke_Warning(String smoke_Warning) {
        this.smoke_Warning = smoke_Warning;
    }

    public String getHydrogen_warning() {
        return hydrogen_warning;
    }

    public void setHydrogen_warning(String hydrogen_warning) {
        this.hydrogen_warning = hydrogen_warning;
    }

    public Integer getHyVal() {
        return hyVal;
    }

    public void setHyVal(Integer hyVal) {
        this.hyVal = hyVal;
    }

    public Integer getAmVal() {
        return amVal;
    }

    public void setAmVal(Integer amVal) {
        this.amVal = amVal;
    }

    public Integer getDustVal() {
        return dustVal;
    }

    public void setDustVal(Integer dustVal) {
        this.dustVal = dustVal;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
