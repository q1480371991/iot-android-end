package com.example.myapplication.pojo;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.myapplication.Line_Detection_Activity;
import com.example.myapplication.R;
import com.example.myapplication.Route_Details_Activity;
import com.example.myapplication.ZoomActivity;
import com.github.chrisbanes.photoview.PhotoView;
import com.qmuiteam.qmui.widget.QMUIVerticalTextView;

import java.util.ArrayList;
import java.util.List;

public class MyLineAdapter extends RecyclerView.Adapter<MyLineAdapter.LineViewHolder>  {

    private List<Line> lines;
    private Context context;



    public MyLineAdapter(Context context, List<Line> lines) {

        this.context = context;
        this.lines = lines;
    }
    public List<Line> getLines() {
        return lines;
    }

    public void setLines(List<Line> lines) {
//        this.lines.clear();
        this.lines=lines;
        Log.i("lxl", "this.lines:"+this.lines.toString());
        notifyDataSetChanged();

    }
    @NonNull
    @Override
    public LineViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.line_item, parent, false);
        return new LineViewHolder(view);
    }



    @Override
    public void onBindViewHolder(@NonNull LineViewHolder holder, int position) {
        Line line = lines.get(position);


        Glide.with(context).load(line.getImgurl()).into(holder.photoView);

        if (line.getIsdeal() == 0) {
            holder.textView.setText("破损");
            holder.textView.setTextColor(context.getResources().getColor(R.color.red));
        } else {
            holder.textView.setText("正常");
            holder.textView.setTextColor(context.getResources().getColor(R.color.green));
        }

        holder.itemView.setOnClickListener(v -> {
            Intent intent;
            if (line.getIsdeal() == 0) {
                intent = new Intent(context, Line_Detection_Activity.class);
            } else {
                intent = new Intent(context, Route_Details_Activity.class);
                intent.putExtra("roomid", line.getRoomid());
                intent.putExtra("status", line.getStatus());
            }
            context.startActivity(intent);
        });

        holder.photoView.setOnClickListener(v -> {
            Intent intent = new Intent(context, ZoomActivity.class);
            intent.putExtra("imageResId", line.getImgurl());
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return lines.size();
    }

    static class LineViewHolder extends RecyclerView.ViewHolder {
        PhotoView photoView;
        QMUIVerticalTextView textView;

        LineViewHolder(@NonNull View itemView) {
            super(itemView);
            photoView = itemView.findViewById(R.id.photoView);
            textView = itemView.findViewById(R.id.mytt);
        }
    }


}
