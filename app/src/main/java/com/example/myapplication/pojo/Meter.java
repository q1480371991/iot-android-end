package com.example.myapplication.pojo;

import com.alibaba.fastjson.annotation.JSONField;

public class Meter {

    private Integer id  ;
    @JSONField(name = "img_url")
    private String img_url;
    private String name;
    private String data;

    @Override
    public String toString() {
        return "Meter{" +
                "id=" + id +
                ", img_url='" + img_url + '\'' +
                ", name='" + name + '\'' +
                ", data='" + data + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Meter() {
    }

    public Meter(Integer id, String img_url, String name, String data) {
        this.id = id;
        this.img_url = img_url;
        this.name = name;
        this.data = data;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }


}
