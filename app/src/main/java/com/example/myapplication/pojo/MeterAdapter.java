package com.example.myapplication.pojo;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.example.myapplication.R;
import com.example.myapplication.ZoomActivity;
import com.qmuiteam.qmui.widget.QMUIRadiusImageView;
import com.qmuiteam.qmui.widget.QMUIVerticalTextView;

import java.util.List;

public class MeterAdapter extends ArrayAdapter<Meter> {
    private List<Meter> meters;
    private Context context;
    private int resourceId;

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        Meter meter = getItem(position);
        View view=null;
        Meter_ViewHolder vh=null;

        if (convertView==null){
            view= LayoutInflater.from(getContext()).inflate(resourceId,parent,false);
            QMUIRadiusImageView imageView = view.findViewById(R.id.img);
            TextView nametv = view.findViewById(R.id.name);
            TextView datatv = view.findViewById(R.id.data);

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Context applicationContext = context.getApplicationContext();

                    Intent intent = new Intent(applicationContext, ZoomActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("imageResId",meter.getImg_url());
                    applicationContext.startActivity(intent);
                }
            });

            vh=new Meter_ViewHolder(imageView,nametv,datatv);
        }else{
            view=convertView;
            vh=(Meter_ViewHolder) view.getTag();
        }
        if (vh!=null){
            Glide.with(context).load(meter.getImg_url()).into(vh.getImg());
            vh.getName().setText(meter.getName());
            vh.getData().setText(meter.getData());
        }

        return view;
    }

    public MeterAdapter(Context context, int resourceId, List<Meter> meters){

        super(context,resourceId,meters);
        this.context=context;
        this.resourceId=resourceId;
        this.meters=meters;
    }
}
