package com.example.myapplication.pojo;
//数据工具类
public class Res {
    private  Boolean flag;//请求状态
    private  String msg;
    private Object data;
    private  Integer code;
    public Res(Boolean flag)
    {
        this.flag=flag;
    }
    public Res() {}

    public Res(Boolean flag, String msg, Object data, Integer code) {
        this.flag = flag;
        this.msg = msg;
        this.data = data;
        this.code = code;
    }

    public Boolean getFlag() {
        return flag;
    }

    public void setFlag(Boolean flag) {
        this.flag = flag;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Res(Object data) {
        this.data=data;
    }

    @Override
    public String toString() {
        return "Res{" +
                "flag=" + flag +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                ", code=" + code +
                '}';
    }
}
