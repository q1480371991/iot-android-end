package com.example.myapplication.pojo;


import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.myapplication.R;
import com.example.myapplication.ZoomActivity;
import com.qmuiteam.qmui.widget.QMUIRadiusImageView;
import com.qmuiteam.qmui.widget.QMUIVerticalTextView;

import java.util.List;
public class MyMeterAdapter extends RecyclerView.Adapter<MyMeterAdapter.MeterViewHolder> {
    private List<Meter> meters;
    private Context context;

    public MyMeterAdapter(Context context, List<Meter> meters) {
        this.context = context;
        this.meters = meters;
    }
    public void setMeters(List<Meter> meters){

        this.meters=meters;
        Log.i("lxl", "this.meters:"+this.meters.toString());
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public MeterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.meter_item, parent, false);
        return new MeterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MeterViewHolder holder, int position) {
        Meter meter = meters.get(position);

        Glide.with(context).load(meter.getImg_url()).into(holder.img);
        holder.name.setText(meter.getName());
        holder.data.setText(meter.getData());

        holder.img.setOnClickListener(v -> {
            Intent intent = new Intent(context, ZoomActivity.class);
            intent.putExtra("imageResId", meter.getImg_url());
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return meters.size();
    }

    static class MeterViewHolder extends RecyclerView.ViewHolder {
        QMUIRadiusImageView img;
        TextView name;
        TextView data;

        MeterViewHolder(@NonNull View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.img);
            name = itemView.findViewById(R.id.name);
            data = itemView.findViewById(R.id.data);
        }
    }
}
