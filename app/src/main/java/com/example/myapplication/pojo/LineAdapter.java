package com.example.myapplication.pojo;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.example.myapplication.Line_Detection_Activity;
import com.example.myapplication.R;
import com.example.myapplication.Route_Details_Activity;
import com.example.myapplication.ZoomActivity;
import com.github.chrisbanes.photoview.PhotoView;
import com.qmuiteam.qmui.widget.QMUIVerticalTextView;

import java.util.ArrayList;
import java.util.List;

public class LineAdapter extends ArrayAdapter<Line> {
    private List <Line> lines;



    private Context context;
    private int resourceId;

    public List<Line> getLines() {
        return lines;
    }

    public void setLines(List<Line> lines) {
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//
//            }
//        });
        this.lines.clear();
        this.lines.addAll(lines);
        Log.i("lxl", "lineAdapter lines:"+this.lines.toString());
        notifyDataSetChanged();

    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Line line = getItem(position);
        View view=null;
        Line_ViewHolder vh=null;
        if (convertView==null){

            view= LayoutInflater.from(getContext()).inflate(resourceId,parent,false);
            PhotoView photoView = view.findViewById(R.id.photoView);
            QMUIVerticalTextView textView = view.findViewById(R.id.mytt);
            photoView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Context applicationContext = context.getApplicationContext();

                    Intent intent = new Intent(applicationContext, ZoomActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("imageResId",line.getImgurl());
                    applicationContext.startActivity(intent);
                }
            });
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Context applicationContext = context.getApplicationContext();
                    Intent intent = new Intent(context, Route_Details_Activity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("roomid",line.getRoomid());
                    intent.putExtra("status",line.getStatus());
                    applicationContext.startActivity(intent);
                }
            });
            vh=new Line_ViewHolder(photoView,textView);
        }else{
//            view=convertView;
//            vh=(Line_ViewHolder) view.getTag();
        }
        if (vh!=null){
            Glide.with(context).load(line.getImgurl()).into(vh.getPhotoView());

            if (line.getIsdeal()==0){
                QMUIVerticalTextView textView = vh.getTextView();
                textView.setText("未处理");


                textView.setTextColor(ContextCompat.getColor(this.getContext(), R.color.red));

            }else{

                QMUIVerticalTextView textView = vh.getTextView();
                textView.setText("已处理");
                textView.setTextColor(ContextCompat.getColor(this.getContext(), R.color.green));

            }

        }


        return view;
    }

    public LineAdapter(Context context, int resourceId, List<Line> lines){
        super(context,resourceId,lines);
        this.context=context;
        this.resourceId=resourceId;
        this.lines=lines;
    }
}
