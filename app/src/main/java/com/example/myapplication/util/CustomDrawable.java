package com.example.myapplication.util;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

public class CustomDrawable extends Drawable {

    private Paint paint;

    public CustomDrawable() {
        paint = new Paint();
        paint.setColor(Color.RED); // 设置线条颜色
        paint.setStrokeWidth(2f); // 设置线条宽度
        paint.setStyle(Paint.Style.STROKE); // 设置为描边模式
    }

    @Override
    public void draw(Canvas canvas) {
        Rect bounds = getBounds();
        canvas.drawRect(bounds, paint); // 绘制矩形框线
    }

    @Override
    public void setAlpha(int alpha) {
        // 设置透明度
    }

    @Override
    public void setColorFilter(ColorFilter colorFilter) {
        // 设置颜色过滤器
    }

    @Override
    public int getOpacity() {
        // 返回透明度信息
        return PixelFormat.TRANSLUCENT;
    }
}
