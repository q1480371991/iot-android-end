package com.example.myapplication.util;

import com.example.myapplication.pojo.IotDataObj;

import java.util.ArrayList;
import java.util.Date;

public class analog_Data {
    public static ArrayList<IotDataObj> getHistoricalData(){
        ArrayList<IotDataObj> datas = new ArrayList<>();

        String time=new Date().toString();
        datas.add(new IotDataObj(1,null,null,1,1,null,null,null,1,1,1,time));
        datas.add(new IotDataObj(2,null,null,2,2,null,null,null,2,2,2,time));
        datas.add(new IotDataObj(3,null,null,3,3,null,null,null,3,3,3,time));
        return datas;

    }
}
