package com.example.myapplication.util;

import android.content.Context;
import android.widget.Toast;
import com.qmuiteam.qmui.widget.dialog.QMUIDialog;
import com.qmuiteam.qmui.widget.dialog.QMUIDialogAction;

public class MyQMUIDialog {
    public static void MessageDialogBuilder(Context context) {
        new QMUIDialog.MessageDialogBuilder(context)
                .setMessage("标题")
                .setTitle("确认要发送吗?")
                .addAction("取消", new QMUIDialogAction.ActionListener() {
                    @Override
                    public void onClick(QMUIDialog dialog, int index) {
                        Toast.makeText(context, "取消", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                })
                .addAction("确认", new QMUIDialogAction.ActionListener() {
                    @Override
                    public void onClick(QMUIDialog dialog, int index) {
                        Toast.makeText(context, "确认", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                })
                .show();
    }

}
