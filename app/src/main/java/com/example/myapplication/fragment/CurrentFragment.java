package com.example.myapplication.fragment;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.alibaba.fastjson.JSON;
import com.example.myapplication.R;
import com.example.myapplication.pojo.IotDataObj;
import com.example.myapplication.pojo.Meter;
import com.example.myapplication.pojo.MeterAdapter;
import com.example.myapplication.pojo.MyMeterAdapter;
import com.example.myapplication.pojo.Res;
import com.qmuiteam.qmui.widget.roundwidget.QMUIRoundButton;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CurrentFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CurrentFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private RecyclerView meter_lv;
    private MyMeterAdapter meterAdapter;
    private List<Meter> meters;
    private QMUIRoundButton refresh_bt;

    public CurrentFragment() {
    }


    public static CurrentFragment newInstance() {
        CurrentFragment fragment = new CurrentFragment();
//        Bundle args = new Bundle();传数据
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        updatedata();
//        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
//        }




    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_current, container, false);
        meter_lv= view.findViewById(R.id.meter_listview);
        refresh_bt=view.findViewById(R.id.myrefresh_bt);
                refresh_bt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.i("lxl", "click");
                        Meter meter = new Meter(null, "http://101.34.248.186:8080/mypath/iot/imgs/meter_reading_imgs/yibiao.jpg", "水压表", "10");
                        ArrayList<Meter> meters1 = new ArrayList<>();
                        meters1.addAll(meters);
                        meters1.add(meter);
                        meters.add(meter);
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                meterAdapter.setMeters(meters1);
                            }
                        });
                    }
                });

        initData();

        return view;
    }

    private void initData(){
        meters=new ArrayList<>();
        meters.add(new Meter(null,"http://101.34.248.186:8080/mypath/iot/imgs/meter_reading_imgs/yibiao.jpg","水压表","10"));
//        meters.add(new Meter(null,"http://101.34.248.186:8080/mypath/iot/imgs/meter_reading_imgs/1.jpg","仪器1","10"));
//        meters.add(new Meter(null,"http://101.34.248.186:8080/mypath/iot/imgs/meter_reading_imgs/2.jpg","仪器2","10"));
//        meters.add(new Meter(null,"http://101.34.248.186:8080/mypath/iot/imgs/meter_reading_imgs/3.jpg","仪器3","10"));
//        meters.add(new Meter(null,"http://101.34.248.186:8080/mypath/iot/imgs/meter_reading_imgs/4.jpg","仪器4","10"));
        meterAdapter=new MyMeterAdapter(getContext(),meters);
        meter_lv.setLayoutManager(new LinearLayoutManager(getContext())); // 使用线性布局管理器，你可以根据需要选择其他布局管理器
        meter_lv.setAdapter(meterAdapter);

    }

    private void updatedata(){
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("text/plain");
        RequestBody body = RequestBody.create(mediaType, "");
        Request request = new Request.Builder()
                .url("http://101.34.248.186:8084/getMeterReadings")
                .method("POST", body)
                .build();
        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                Log.e("lxl",e.getMessage());
                Log.e("lxl","error");

            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {

                String res = response.body().string();
                Res r = JSON.parseObject(res, Res.class);

                if (200==(r.getCode())){
                    Object data = r.getData();
                    List<Meter> newmeters = JSON.parseArray(JSON.toJSONString(data), Meter.class);
                    Log.e("lxl","arr:"+newmeters.toString());
                    FragmentActivity activity = getActivity();
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            //渲染ui
                            meterAdapter=new MyMeterAdapter(getActivity(),newmeters);
                            meter_lv.setAdapter(meterAdapter);

                        }
                    });
                }



            }
        });

    }
}