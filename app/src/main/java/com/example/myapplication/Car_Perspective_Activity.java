package com.example.myapplication;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.SurfaceView;
import android.view.View;

import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ext.rtmp.RtmpDataSource;
import com.google.android.exoplayer2.ext.rtmp.RtmpDataSourceFactory;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.qmuiteam.qmui.widget.QMUITopBar;

public class Car_Perspective_Activity extends AppCompatActivity {
    private QMUITopBar mTopBar;
    private SimpleExoPlayer player;
    private SurfaceView surfaceView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_perspective);
        ActionBar actionBar = getSupportActionBar();


        surfaceView = findViewById(R.id.surface_view);


        if (actionBar != null) {
            actionBar.hide();
        }
        mTopBar=findViewById(R.id.c_ptopbar);
        QMUIStatusBarHelper.translucent(this);// 沉浸式状态栏
        initTopBar();//初始化状态栏
    }

    private void initTopBar() {
        mTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.blue));
        mTopBar.setTitleGravity(Gravity.LEFT);
        mTopBar.addLeftBackImageButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
//                overridePendingTransition(R.anim.slide_still, R.anim.slide_out_right);
            }
        });

        mTopBar.setTitle("小车视角");
    }

    private void initPlayer(){
        String videoUrl="rtmp://101.34.248.186:1935/live/test";
        // 初始化ExoPlayer
        AdaptiveTrackSelection.Factory trackSelectionFactory = new AdaptiveTrackSelection.Factory();
        DefaultTrackSelector trackSelector = new DefaultTrackSelector(/* context= */ this, trackSelectionFactory);

        player = new SimpleExoPlayer.Builder(/* context= */ this)
                .setTrackSelector(trackSelector)
                .build();


        // 创建一个RTMP数据源工厂
        RtmpDataSource.Factory rtmpDataSourceFactory = new RtmpDataSource.Factory();

        // 创建一个数据源工厂
        DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(this,
                Util.getUserAgent(this, "MyApplication"));

        // 创建一个媒体源
        MediaSource videoSource = new ProgressiveMediaSource.Factory(dataSourceFactory)
                .createMediaSource(MediaItem.fromUri(videoUrl));

        // 准备播放器并设置媒体源
        player.setMediaSource(videoSource);

        // 准备播放器
        player.prepare();

        // 将播放器关联到视图
        player.setVideoSurfaceView(surfaceView);

        // 开始播放
        player.setPlayWhenReady(true);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        // 释放播放器
        player.release();
    }
}